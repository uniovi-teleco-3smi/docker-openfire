# Contenedor con servidor de Mensajería Instantánea Openfire (XMPP) para la asignatura Servicios Multimedia e Interactivos

Clona el repositorio y modifícalo según tus preferencias.

### Parámetros que pueden modificarse

 * No se recomienda cambiar ninguno.

Los parámetros deben cambiarse en el fichero 'docker-compose.yml' antes de iniciar el contenedor.

### Arrancar el contenedor

Para iniciar el contenedor (ejecutar su imagen):

```console
sudo docker-compose up -d
```

Para parar/arrancar/reiniciar/eliminar el contenedor:

```console
sudo docker-compose stop/start/restart/down
```

El contenedor volverá a arrancar automáticamente si se reinicia la máquina anfitriona.
